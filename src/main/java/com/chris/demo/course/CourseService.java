package com.chris.demo.course;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {

	@Autowired
	CourseRepository courseRepository; 
	  

	public List<Course> getAllCourses(String topicId){ 
		 List<Course> courses=new ArrayList<>();
		 courseRepository.findByTopicId(topicId).forEach(courses::add);
		 return courses;
	 }
	 
	 public Course getTopic(String id) {
 		 return courseRepository.findOne(id);
	 }

	public void addCourse(Course course) {
 		courseRepository.save(course);
	}
 

	public void deleteTopic(String id) {
 		courseRepository.delete(id);
	}

	public void updateCourse(Course course) {
		courseRepository.save(course); 
		
	}
 
}
