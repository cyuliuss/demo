package automationFramework;
 

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chris.demo.DemoApplication;
import com.chris.demo.topic.Topic;
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DemoApplication.class) 
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT) 
public class TestNG  extends AbstractTestNGSpringContextTests{

    @Autowired
    private TestRestTemplate restTemplate;
    

	@Test
	public void f() {
		ResponseEntity<Topic[]> responseEntity=restTemplate.getForEntity("/topics", Topic[].class);
        Topic[] topics = responseEntity.getBody();
        List<Topic> tpc=Arrays.asList(topics);
        HttpStatus statusCode = responseEntity.getStatusCode();
         
        assertThat(statusCode).isEqualTo(HttpStatus.OK);
        assertThat(tpc.get(0).getId()).isEqualTo("Spring"); 
        assertThat(tpc).isNotEmpty();
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

}
