package com.chris.demo.course;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.chris.demo.topic.Topic;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseController.class)
public class CourseControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	CourseService courseService;
	
	Topic tpc = new Topic("Spring","Spring framework asdgdfg ok"," descriptiocvdfn"); 
	List<Course> mockCourse= Arrays.asList(new Course("Spring detail 1","Spring detail 1"," descriptiocvdfn","Spring"));
	
	Course mockCourse2= new Course("Spring detail 1","Spring detail 1"," descriptiocvdfn",tpc);
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAllCourses() throws Exception{
		Mockito.when(courseService.getAllCourses(Mockito.anyString())).thenReturn(mockCourse);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/topics/Spring/courses").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());
		String expected = "[{\"id\":\"Spring detail 1\",\"name\":\"Spring detail 1\",\"description\":\" descriptiocvdfn\",\"topic\":{\"id\":\"Spring\",\"name\":\"\",\"description\":\"\"}}]";

		System.out.println(result.getResponse().getContentAsString());
		// {"id":"Course1","name":"Spring","description":"10 Steps, 25 Examples and 10K Students","steps":["Learn Maven","Import Project","First Example","Second Example"]}
//
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void testGetCourse() throws Exception {
		Mockito.when(courseService.getTopic(Mockito.anyString())).thenReturn(mockCourse2);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/topics/Spring/courses/Spring%20detail%201/").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = "{\"id\":\"Spring detail 1\",\"name\":\"Spring detail 1\",\"description\":\" descriptiocvdfn\",\"topic\":{\"id\":\"Spring\",\"name\":\"Spring framework asdgdfg ok\",\"description\":\" descriptiocvdfn\"}}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
		
	}

	@Test
	public void testAddCourse() throws Exception{
		Course mockCourse3= new Course("Spring detail 2","Spring detail 2","",tpc);
		doNothing().when(courseService).addCourse(isA(Course.class));
		courseService.addCourse(mockCourse3);
		
		verify(courseService).addCourse(mockCourse3);
	}

	@Test
	public void testUpdateCourse() {
		Course mockCourse3= new Course("Spring detail 2","Spring detail 2","",tpc);
		doNothing().when(courseService).updateCourse(isA(Course.class));
		courseService.updateCourse(mockCourse3);
		
		verify(courseService).updateCourse(mockCourse3);
	}

	@Test
	public void testDeleteTopic() {
		Course mockCourse3= new Course("Spring detail 2","Spring detail 2","",tpc);
		doNothing().when(courseService).deleteTopic(isA(String.class));
		courseService.addCourse(mockCourse3);
		courseService.deleteTopic("Spring detail 2");
		
		verify(courseService).deleteTopic("Spring detail 2");
	}

}
