package com.chris.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.chris.demo.topic.Topic;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;
    
	@Test
	public void contextLoads() {
        ResponseEntity<Topic[]> responseEntity=restTemplate.getForEntity("/topics", Topic[].class);
        Topic[] topics = responseEntity.getBody();
        List<Topic> tpc=Arrays.asList(topics);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertEquals(HttpStatus.OK, statusCode);
        assertNotNull(tpc);
        assertEquals("Spring", tpc.get(0).getId());

	}

	@Test
	public void contextLoads2() {
        ResponseEntity<Topic[]> responseEntity=restTemplate.getForEntity("/topics", Topic[].class);
        Topic[] topics = responseEntity.getBody();
        List<Topic> tpc=Arrays.asList(topics);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertEquals(HttpStatus.OK, statusCode);
        assertNotNull(tpc);
        assertEquals("Spring", tpc.get(0).getId());

	}

}
