/**
 * 
 */
package com.chris.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.chris.demo.course.Course;
import com.chris.demo.course.CourseRepository;
import com.chris.demo.course.CourseService;
import com.chris.demo.topic.Topic;

/**
 * @author Chris
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServiceTest {

	   

	@Autowired
	private CourseService courseService;
	 @Autowired
	private CourseRepository courseRepository;
	
	/**
	 * @throws java.lang.Exception
	 */
//	@Before
//	public void setUp() throws Exception {
////		courseRepository = Mockito.mock(CourseRepository.class);
////		courseService = new CourseService();
//	}

	/**
	 * Test method for {@link com.chris.demo.course.CourseService#getAllCourses(java.lang.String)}.
	 */
	@Test
	public void testGetAllCourses() {
		List<Course> lcourse=courseRepository.findByTopicId("Spring");
		
//		courseRepository.findOne();
		assertNotNull(lcourse);
		
	}

	/**
	 * Test method for {@link com.chris.demo.course.CourseService#getTopic(java.lang.String)}.
	 */
	@Test
	public void testGetTopic() {
		 

		List<Course> lcourse=courseRepository.findByTopicId("Foo");
		
		assertEquals(0, lcourse.size());
	}

	/**
	 * Test method for {@link com.chris.demo.course.CourseService#addCourse(com.chris.demo.course.Course)}.
	 */
	@Test
	public void testAddCourse() {
		fail("Not yet implemented");
	}

}
